<================================= DOCUMENTATION ==========>

made by Pavel Sulga (27.08.2019)
email: pavelaizen@gmail.com
linkedin: https://www.linkedin.com/in/psulga/
goal: demonstrate technical skills and approach while completing assignment

<======= Answering the question regarding back-end sorting =============>

Question: If sorting and filtering is being made on the /cars and /users endpoit root,
justify Your selection with few words!

Answer: Most of the clients nowadays aren't real users but rather a third-party programs,
which doesn't have web interface to operate data visually -> in this case back-end root endpoint sorting
is way more appropriate approach.

<========Technologies, libraries and frameworks used ===========>

Gradle
Spring Data JPA
Spring Data REST
Spring Web
Spring Boot
Hibernate
Mustache
Spring Security
H2 Embedded Database
Lombok
JUnit
Mockito
Bootstrap
jQuery

<========== REST APIs ==============>

host: localhost
port: 8080

[POST] - /api/cars - Create a car manually using API
[POST] - /api/users - Create a user manually using API
[GET] - /api/users - Get all Users from the Database
[GET] - /api/cars - Get all Cars from the Database
[GET] - /api/users/{id} - Get User from the Database by specifying User Id
[GET] - /api/cars/{id} - Get Car from the Database by specifying Car Id
[GET] - /api/users/{id}/cars - Get Cars from the Database by specifying User Id


<========== Searching, Sorting, Filtering and Pagination =============>

/api/users?page=0 - Return first(0) page with a capacity of 10 objects
/api/users?find=Allik - Returns all users with name LIKE %Allik%
/api/users?sort=id&order=asc - Return first page of users, sorted by Id in the Ascending order
/api/users?find=Allik&sort=name&order=desc - Return first page users with names LIKE %Allik%, sorted by name in the descending order

/api/cars?page=0 - Return first(0) page with a capacity of 10 objects
/api/cars?find=Lambo - Returns all cars with make LIKE %Lambo%
/api/cars?sort=id&order=asc - Return first page of cars, sorted by Id in the Ascending order
/api/cars?find=337AAA - Return car with numberplate LIKE %337AAA%

Syntax:

page={variable}
find={variable}
order=asc OR desc
sort={variable}

<==================== H2 Database =======================================>

Data is being presented from static variables
Database is being initialized on the program start from schema.sql file

<============================== Unit Tests =============================>

Unit tests are performed in the src/test/java/com/it/hansab/controller/UserAndCarControllerTests.java file

<========================== Spring Security ============================>

username: user
password: password

<======================= THE END ========================================>

package com.it.hansab.controller;

import com.it.hansab.model.entity.Car;
import com.it.hansab.model.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CarController {

    @Autowired
    private CarRepository carRepository;

    //========================= Adding car manually to Database ============================================//
    @PostMapping("/cars")
    public ResponseEntity<Car> createCar(@Valid @RequestBody Car car) {
        final Car result = carRepository.save(car);
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    //====================== Retrieving a specific car by Id ===============================================//
    @GetMapping("/cars/{id}")
    public ResponseEntity<Car> getCarsById(@PathVariable final Long id)
    {
        return ResponseEntity.ok(carRepository.findById(id).get());
    }

    //============= Retrieving all cars that specific user has by UserId (foreign key) ======================//
    @GetMapping("/users/{id}/cars")
    public ResponseEntity<Collection<Car>> getAllCarsByUserId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(carRepository.findByUserId(id));
    }

    //===== Retrieve all cars from database OR apply Search,Filter and Pagination mechanism ==============//
    @GetMapping("/cars")
    public List<Car> findCars(@RequestParam Optional<String> find,
                             @RequestParam Optional<Integer> page,
                             @RequestParam(required = false, defaultValue = "Asc") String order,
                             @RequestParam Optional<String> sort)
    {
        return carRepository.findByNumberplate(find.orElse("_"),
                new PageRequest(page.orElse(0), 10,
                        Sort.Direction.fromString(order), sort.orElse("numberplate")) {
                });
    }
}

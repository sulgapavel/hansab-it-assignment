package com.it.hansab.controller;

import com.it.hansab.model.entity.User;
import com.it.hansab.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    //=========================== Adding user manually to Database ============================================//
    @PostMapping("/users")
    public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        final User result = userRepository.save(user);
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    //========== Retrieve all users from database OR apply Search,Filter and Pagination ==================//
    @GetMapping("/users")
    public List<User> findUserByName(@RequestParam Optional<String> find,
                                     @RequestParam Optional<Integer> page,
                                     @RequestParam(required = false, defaultValue = "Asc") String order,
                                     @RequestParam Optional<String> sort)
    {
        return userRepository.findByName(find.orElse("_"),
                new PageRequest(page.orElse(0),10,
                        Sort.Direction.fromString(order), sort.orElse("name")));
    }

    //============================== Retrieving specific user by Id =========================================//
    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable final Long id)
    {
        return ResponseEntity.ok(userRepository.findById(id).get());
    }


}

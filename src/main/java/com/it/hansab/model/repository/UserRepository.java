package com.it.hansab.model.repository;

import com.it.hansab.model.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE name LIKE %?1%")
    List<User> findByName(String name, Pageable pageable);

}

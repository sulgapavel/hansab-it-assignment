package com.it.hansab.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy="user",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Collection<Car> cars;

    public User(String name){ this.name = name;}

    public User(Long id,String name){
        this.id = id;
        this.name = name;
    }

}

CREATE TABLE Users (
  id varchar(36) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(36) NOT NULL,
  PRIMARY KEY (id)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE Cars (
  id varchar(36) unsigned NOT NULL AUTO_INCREMENT,
  user_id varchar(36) unsigned NOT NULL,
  make varchar(36) NOT NULL,
  model varchar(36) NOT NULL,
  numberplate varchar(6) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT cars_ibfk_1 FOREIGN KEY (user_id) REFERENCES Users (id)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
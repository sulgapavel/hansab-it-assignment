package com.it.hansab.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.it.hansab.model.entity.Car;
import com.it.hansab.model.entity.User;
import com.it.hansab.model.repository.CarRepository;
import com.it.hansab.model.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserAndCarControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private UserRepository userRepository;

    //==== Mocking the get method without authorization to test the security ====//
    @Test
    public void securityCheck() throws Exception {
        this.mvc.perform(get("/api/users"))
                .andExpect(status().isUnauthorized());
    }

    //==== As security is implemented, mocking user to authorize an authenticate and get list of all Users ====//
    @Test
    @WithMockUser
    public void getAllUsers() throws Exception{
        this.mvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    //==== As security is implemented, mocking user to authorize and authenticate and get list of all Cars ====//
    @Test
    @WithMockUser
    public void getAllCars() throws Exception{
        this.mvc.perform(get("/api/cars"))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    //==== As security is implemented, mocking user to authorize and authenticate and get specific User by Id ====//
    @Test
    @WithMockUser
    public void getUserById() throws Exception {
        final ResultActions perform = mvc.perform(get("/api/users/{id}", 4L))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    //==== As security is implemented, mocking user to authorize and authenticate and get specific Car by Id ====//
    @Test
    @WithMockUser
    public void getCarById() throws Exception {
        final ResultActions perform = mvc.perform(get("/api/cars/{id}", 6L))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    //==== Mocking the post method without authorization and authentication to test the security ====//
    @Test
    public void createCar() throws Exception {

        final Car car = new Car(999L,"Audi","A8","998AUD",new User(999L,"Marat Kerst"));

        final ResultActions perform = mvc.perform(post("/api/cars")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(car)));

        perform.andExpect(status().isForbidden());
    }

    //==== As security is implemented, mocking user to authorize and authenticate and get all cars by User_Id ====//
    @Test
    @WithMockUser
    public void getCarsByUserId() throws Exception{
        final ResultActions perform = mvc.perform(get("/api/users/{id}/cars", 6L))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }
}
